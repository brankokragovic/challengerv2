<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
class Follow extends Model
{
    protected $fillable = ['target_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //https://www.sitepoint.com/real-time-laravel-notifications-follows-sure-stream/
    // iz blejda pokupiti logiku Auth::User()->isFollowing($user->id)
}