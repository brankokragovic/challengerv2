<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Follow;
use Illuminate\Support\Facades\Auth;
class FollowController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $user  = User::where('id','!=', $id )->get();
        return $user;
    }

    public function follow(User $user)
    {
        if (!Auth::user()->isFollowing($user->id))
        {
            // Create a new follow instance for the authenticated user
            Auth::user()->follows()->create([
                'target_id' => $user->id,
            ]);

            return response('You are now follow ' . $user->name,200);
        } else
        {
            return response( 'You are already following this person',403);
        }
    }

    public function unfollow(User $user)
    {
        if (Auth::user()->isFollowing($user->id))
        {
            $follow = Auth::user()->follows()->where('target_id', $user->id)->first();
            $follow->delete();

            return response('You are no longer follow '. $user->name,200);
        } else
        {
            return response('You are not following this person',403);
        }
    }

    //https://www.sitepoint.com/real-time-laravel-notifications-follows-sure-stream/
}
