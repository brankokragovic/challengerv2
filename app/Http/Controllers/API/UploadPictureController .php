<?php

namespace App\Http\Controllers\API;

use App\ProfilePicture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class UploadPictureController extends Controller
{
    public function uploadProfilePicture(Request $request)
    {
        $data = $request->all();
        $rules = [
            'picture' => 'required|max:100040|image|mimes:jpeg,jpg,png,bmp,gif,svg'];
        $validator = Validator($data, $rules);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
            //ovde logika za redirektovanj trenutno redirektuje na laravel homepage

        } else {

            $file = $data['picture'];
            $extension = pathinfo($file->getClientOriginalExtension(), PATHINFO_FILENAME);
            $input = hash('md5', $file->getClientOriginalName());
            $destinationPath = public_path('uploads/pictures');
            $file->move($destinationPath, $input. '.' . $extension);

            $picture = new ProfilePicture();
            $picture->user_id = Auth::user()->id;
            $picture->path = $input. '.' . $extension;
            $picture->save();

            $responce = 'Profile picture have been upload successfully';
            return response($responce,200);
        }
    }


    public function getProfilePicture()
    {
        $id = Auth::user()->id;
        $path = ProfilePicture::select('path')->where('user_id',$id)->get();
        return $path;

    }

    public function deleteProfilePicture(Request $request)
    {
        $id = Auth::user()->id;

        if(Auth::user())
        {
            if(count(ProfilePicture::where('user_id',$id)->get()) == 0)
            {
                $responce = 'You do not have profile picture';
                return response($responce, 200);
            }
            else {
                ProfilePicture::where('user_id', $id)->delete();
                $responce = 'Profile picture have been deleted successfully';
                return response($responce, 200);
            }
        }
    }
}
