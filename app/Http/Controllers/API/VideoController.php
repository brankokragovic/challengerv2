<?php

namespace App\Http\Controllers\API;

use App\Video;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use FFMpeg;
class VideoController extends Controller
{
    public function uploadVideo(Request $request)
    {
        $data = $request->all();
        $rules = [
            'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required'];
        $validator = Validator($data, $rules);

        if ($validator->fails()) {
            return redirect()
                ->back()          //TODO ovde logika za redirektovanje trenutno redirektuje na laravel homepage
                ->withErrors($validator)
                ->withInput();
        } else {
           dd($data['video']);
            $file = $data['video'];
            $extension = pathinfo($file->getClientOriginalExtension(), PATHINFO_FILENAME);
            $input = hash('md5', $file->getClientOriginalName());
            $destinationPath = public_path('uploads/videos');
            $file->move($destinationPath, $input. '.' . $extension);

            $video = new Video();
            $video->user_id = Auth::user()->id;
            $video->path = $input. '.' . $extension;
            $video->name = Auth::user()->name . "'s " . " " . 'video';
            $video->save();

            $responce = 'Video have been created successfully';
            return response($responce,200);
        }
    }


    public function getAllUserVideos()
    {
        $id = Auth::user()->id;
        $path = Video::select('path')->where('user_id',$id)->get();
        return $path;

    }

//    public function deleteVideo(Request $request)
//    {
//        $id = Auth::user()->id;
//
//        if(Auth::user())
//        {
//            if(count(Video::where('user_id',$id)->get()) == 0)
//            {
//                $responce = 'You do not have profile picture';
//                return response($responce, 200);
//            }
//            else
//            {
//                Video::where('id', $id)->delete();
//                $responce = 'Video have been deleted successfully';
//                return response($responce, 200);
//            }
//        }
//    }
}
