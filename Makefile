.PHONY: up down build rebuild workspace list workspace

up:
	docker-compose up -d

down:
	docker-compose down

build-app:
	docker build -f ./docker/app.dockerfile -t kadza/app-api .

build-web:
	docker build -f ./docker/web.dockerfile -t kadza/app-web .

build: build-app build-web

rebuild: build

list:
	docker-compose ps

workspace:
	docker-compose exec app bash