<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'API\UserController@register')->name('register');
Route::post('login', 'API\UserController@login')->name('login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('logout', 'API\UserController@logout');
    Route::get('user', 'API\UserController@user');
    Route::post('uploadvideo','API\VideoController@uploadVideo');
    Route::get('videos','API\VideoController@getAllUserVideos');//User all videos
//    Route::delete('videos/{id}','API\VideoController@deleteVideo');//Delete video
    Route::get('/users', 'API\FollowController@index');
    Route::post('/follow/{user}', 'API\FollowController@follow');
    Route::delete('/unfollow/{user}', 'API\FollowController@unfollow');
    Route::post('uploadpicture','API\UploadPictureController@uploadProfilePicture');
    Route::get('getprofilepicture','API\UploadPictureController@getProfilePicture');
    Route::delete('deleteprofilepicture','API\UploadPictureController@deleteProfilePicture');
});