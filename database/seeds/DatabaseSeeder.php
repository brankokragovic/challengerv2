<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => 'branko' . $i,
                'email' => 'branko87' . $i . '@gmail.com',
                'password' => bcrypt('branko2018!'),
                'nickname' => 'bran.k.o' . $i
            ]);
        }

        for ($i = 0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => 'uros' . $i,
                'email' => 'dot.kadza' . $i . '@gmail.com',
                'password' => bcrypt('uros2018!'),
                'nickname' => 'uki' . $i
            ]);
        }
    }
}

